package ExamAPI;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;


    public class WiremockTest {
        @Test
        public void WiremockAPITest() {
            RestAssured.given(new RequestSpecBuilder()
                            .setBaseUri("http://wiremock.atlantis.t-systems.ru/")
                            .addFilter(new RequestLoggingFilter())
                            .addFilter(new ResponseLoggingFilter())
                            .build())
                    .when().get("/automated/exam/agafonovael")
                    .then().assertThat()
                    .statusCode(200)
                    .body("type", Matchers.equalTo("donut"))
                    .body("$", Matchers.hasEntry("name","Cake"))
                    .body("ppu", Matchers.equalTo(0.55F));
    }
}
