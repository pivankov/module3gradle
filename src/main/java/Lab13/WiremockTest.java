package Lab13;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.jupiter.api.Test;

public class WiremockTest {
    @Test
    public void WiremockAPITest() {
        RestAssured.given(new RequestSpecBuilder()
                .setBaseUri("http://wiremock.atlantis.t-systems.ru/")
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .build())
                .when().get("/devops/paivanko/test")
                .then().assertThat()
                .statusCode(202);
    }
}
