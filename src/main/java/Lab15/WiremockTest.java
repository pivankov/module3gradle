package Lab15;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class WiremockTest {
    static RequestSpecification requestSpecification;
    static RequestSpecification requestSpecification2;

    @BeforeAll
    public static void init() {
        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://wiremock.atlantis.t-systems.ru")
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .build();
        requestSpecification2 = new RequestSpecBuilder()
                .setBaseUri("http://wiremock.atlantis.t-systems.ru")
                .addFilter(new RequestLoggingFilter())
                .build();
    }

    @Test
    public void wiremockAPITest1() {
        RestAssured.given()
                .when().get("/els/json/test")
                .then().assertThat()
                .statusCode(200)
                .body("menu.id", Matchers.equalTo("file"))
                .body("menu.popup.menuitem", Matchers.hasSize(3));
    }

    public void wiremockAPITest2() {
        RestAssured.given()
                .when().get("/pavel_ivankov/get")
                .then().assertThat()
                .statusCode(200);
    }
    public void wiremockAPITest3() {
        RestAssured.given()
                .when().get("/pavel_ivankov/get")
                .then().assertThat()
                .statusCode(200);
}
